/**
 * Copyright 2004 - 2022 anaptecs GmbH, Burgstr. 96, 72764 Reutlingen, Germany
 *
 * All rights reserved.
 */
package com.anaptecs.jeaf.fastlane.samples;

import com.anaptecs.jeaf.fastlane.annotations.FastLaneConfig;
import com.anaptecs.jeaf.xfun.annotations.AppInfo;

/**
 * Interface is only used to place annotation that configure JEAF Fast Lane
 */
@AppInfo(
    applicationID = "FastLaneSampleApp",
    applicationName = "JEAF Fast Lane Sample App",
    applicationDescription = "Sample App to show usage of JEAF Fast Lane",
    applicationWebsiteURL = "https://www.anaptecs.de/jeaf",
    applicationCreator = "anaptecs GmbH",
    applicationCreatorURL = "https://www.anaptecs.de")

// If you want to run JEAF Fast Lane with default configuration then you can either not define annotation FastLaneConfig
// at all or just leave it empty as here.
@FastLaneConfig
public interface ServerConfig {
}
