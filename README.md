# JEAF Fast Lane Samples README #

This repository contains the source code of JEAF Fast Lane Samples. JEAF Fast Lane is a bootstrapping project for JEAF services. Using JEAF Fast Lane it is possible to run JEAF Services inside an full featured http environment without having to care about http server and deployments. This repo contains samples that show usage of JEAF Fast Lane.

## Links ##
For further information please refer to:

* [JEAF Fast Lane](https://anaptecs.atlassian.net/l/c/rBjZ1yw0)
* [Maven Repository](https://search.maven.org/artifact/com.anaptecs.jeaf.fast-lane/jeaf-fast-lane-samples)
* [Javadoc](https://anaptecs.atlassian.net/wiki/com.anaptecs.jeaf.fast-lane/jeaf-fast-lane-samples)

## How do I get set up? ##

* Create a clone of this repository on your local machine.
* Execute Maven on the top level project `maven clean install`
