/**
 * Copyright 2004 - 2021 anaptecs GmbH, Burgstr. 96, 72764 Reutlingen, Germany
 *
 * All rights reserved.
 */
package com.anaptecs.jeaf.fastlane.samples;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.anaptecs.jeaf.fastlane.impl.ErrorHandlerFilter;
import com.anaptecs.jeaf.fastlane.impl.FastLaneServer;
import com.anaptecs.jeaf.fastlane.impl.GracefulShutdownFilter;
import com.anaptecs.jeaf.fastlane.impl.WebContainer;
import com.anaptecs.jeaf.fastlane.impl.WebContainerState;
import com.anaptecs.jeaf.fastlane.impl.WebFilterInfo;
import com.anaptecs.jeaf.fastlane.impl.WebServletInfo;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FastLaneServerSample {
  @Test
  public void testFastLaneServer( ) throws IOException {
    FastLaneServer lServer = null;
    try {
      // Create new JEAF Fast Lane Server and start it
      lServer = new FastLaneServer();

      // Check default configuration of workload container.
      WebContainer lWorkloadContainer = lServer.getWorkloadContainer();
      List<WebFilterInfo> lFilters = lWorkloadContainer.getFilters();
      assertEquals(GracefulShutdownFilter.class, lFilters.get(0).getFilterClass());
      assertEquals(ErrorHandlerFilter.class, lFilters.get(1).getFilterClass());
      assertEquals(2, lFilters.size());

      // Check that our REST resource was loaded by JEAF Fast Lane
      assertEquals(RESTTestServiceResource.class, lWorkloadContainer.getRESTResources().get(0));
      assertEquals(1, lWorkloadContainer.getRESTResources().size());
      List<WebServletInfo> lServlets = lWorkloadContainer.getServlets();
      assertEquals(TestServlet.class, lServlets.get(0).getServletClass());
      assertEquals(1, lServlets.size());

      // Start server.
      lServer.start();
      assertEquals(WebContainerState.RUNNING, lServer.getState());

      // Create new http client
      OkHttpClient lClientBuilder = new OkHttpClient();
      OkHttpClient lHttpClient = lClientBuilder.newBuilder().build();

      // Execute simple GET request to our REST service
      Request lRequest = new Request.Builder().url("http://localhost:8090/rest/test").get().build();
      Response lResponse = lHttpClient.newCall(lRequest).execute();
      assertEquals(200, lResponse.code());
      assertEquals("A restful 'Hello' from JEAF Fast Lane!", lResponse.body().string());

      // Also we should send some simple requests to our servlet.
      lRequest = new Request.Builder().url("http://localhost:8090/test").get().build();
      lResponse = lHttpClient.newCall(lRequest).execute();
      assertEquals(200, lResponse.code());
      assertEquals("Test Servlet running in JEAF Fast Lane!", lResponse.body().string());

      lRequest = new Request.Builder().url("http://localhost:8090/testservlet").get().build();
      lResponse = lHttpClient.newCall(lRequest).execute();
      assertEquals(200, lResponse.code());
      assertEquals("Test Servlet running in JEAF Fast Lane!", lResponse.body().string());

      // Shutdown server again.
      lServer.shutdown(0);
      lServer = null;
    }
    finally {
      if (lServer != null) {
        lServer.shutdown(-1);
      }
    }
  }
}
