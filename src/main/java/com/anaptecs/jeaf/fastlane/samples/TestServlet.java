/**
 * Copyright 2004 - 2020 anaptecs GmbH, Burgstr. 96, 72764 Reutlingen, Germany
 *
 * All rights reserved.
 */
package com.anaptecs.jeaf.fastlane.samples;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(asyncSupported = false, loadOnStartup = 1, urlPatterns = { "/test", "/testservlet" })
public class TestServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @Override
  protected void service( HttpServletRequest request, HttpServletResponse response ) throws IOException {
    response.getWriter().append("Test Servlet running in JEAF Fast Lane!");
    response.setStatus(200);
  }
}