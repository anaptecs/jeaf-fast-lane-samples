/**
 * Copyright 2004 - 2021 anaptecs GmbH, Burgstr. 96, 72764 Reutlingen, Germany
 *
 * All rights reserved.
 */
package com.anaptecs.jeaf.fastlane.samples;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Very very basic implementation of a REST resource.
 */
@Path("/test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class RESTTestServiceResource {
  /**
   * {@link RESTTestService#getResource()}
   */
  @GET
  @Produces({ MediaType.TEXT_PLAIN })
  public Response getResource( ) {
    String lContent = "A restful 'Hello' from JEAF Fast Lane!";
    return Response.status(Response.Status.OK).entity(lContent).build();
  }
}
